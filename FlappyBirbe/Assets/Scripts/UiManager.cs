﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {
    int score;
    Text text;

    void Awake()
    {
        text = GetComponent<Text>();

    }

    void Update()
    {
        score = Score.amount;
        text.text = "SCORE:" + score;
    }
}