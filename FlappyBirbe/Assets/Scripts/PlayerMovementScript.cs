﻿using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour
{
    private Rigidbody2D Myrigidbody;
    public const float SPEED = 10.0f;
    public const float JUMP_FORCE = 10.5f;
    private Vector2 velocityVector = new Vector2(SPEED, 0);
    private float jumpTimer = 0;
    private const float TIME_BETWEEN_JUMPS = 1.05f;

    // Use this for initialization
    void Start()
    {
        Myrigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckJump();
        Suicide();
    }

    void FixedUpdate()
    {
        MovePlayerToRight();
    }

    void Suicide()
    {
        if (Input.GetKey(KeyCode.R))
        {
            LoadLevel();
        }
    }

    // Handles player movement to right
    void MovePlayerToRight()
    {
        velocityVector.y = Myrigidbody.velocity.y;
        Myrigidbody.velocity = velocityVector;
    }

    void CheckJump()
    {
        if (jumpTimer <= 0 &&
            (Input.GetKey(KeyCode.Space) || Input.touchCount > 0))
        {
            jumpTimer = TIME_BETWEEN_JUMPS;
            Myrigidbody.AddForce(Vector2.up * JUMP_FORCE, ForceMode2D.Impulse);
        }

        jumpTimer = jumpTimer - Time.deltaTime;
    }

    void LoadLevel()
    {
        Application.LoadLevel("TestScene");
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            LoadLevel();
        }
    }
}