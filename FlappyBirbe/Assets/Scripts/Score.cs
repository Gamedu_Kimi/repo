﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

   public static int amount;

    void Start()
    {
        amount = 0;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        amount = amount + 1;
    }
}
